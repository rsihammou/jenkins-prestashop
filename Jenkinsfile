node {
    def image = "registry.gitlab.com/rsihammou/jenkins-prestashop/prestashop"

    stage('Preparation') {
        sh 'rm -rf reports'
        sh 'mkdir reports'
        checkout scm
        sh 'composer install --optimize-autoloader --no-progress --no-interaction --no-ansi --no-scripts'
    }

    stage('Validate') {
        sh 'bash tests/check_file_syntax.sh'
    }

    stage('Unit Tests') {
        withEnv(["SYMFONY_DEPRECATIONS_HELPER=disabled"]) {
            sh 'vendor/bin/phpunit -c tests/Unit/phpunit.xml --coverage-clover=reports/coverage/coverage.xml --coverage-html=reports/coverage'
        }
        step([$class: 'CloverPublisher', cloverReportDir: 'reports/coverage', cloverReportFileName: 'coverage.xml'])
        publishHTML (target: [
            allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: true,
            reportDir: 'reports/coverage',
            reportFiles: 'index.html',
            reportName: "Coverage Report"
        ])
    }

    stage('Quality') {
        sh 'vendor/bin/phpcs src --report=checkstyle --report-file=reports/checkstyle.xml --standard=PSR2 --extensions=php || exit 0'
        sh 'vendor/bin/phpmd src/ xml phpmd.xml --reportfile reports/pmd.xml || exit 0'
        sh 'vendor/bin/phpcpd src/ --log-pmd reports/cpd.xml --exclude vendor || exit 0'
        recordIssues enabledForFailure: true, tools: [
            checkStyle(pattern: '**/reports/checkstyle.xml'),
            cpd(pattern: '**/reports/cpd.xml'),
            pmdParser(pattern: '**/reports/pmd.xml')
        ]
    }

    stage('Build') {
        docker.withRegistry('https://registry.gitlab.com', 'gitlab-credentials') {
            def dockerImage = docker.build("${image}:latest")
            /* Push the container to the custom Registry */
            dockerImage.push()
        }
    }

    stage('Deploy'){
        ansiblePlaybook(
            inventory: '${Host},',
            playbook: 'playbook.yml',
            credentialsId: 'ansible-private-key',
            disableHostKeyChecking: true
        )
    }

    stage('Acceptance Tests'){
        withEnv(["HOST_URL=http://${HOST}:8090"]) {
            sh 'vendor/bin/phpunit tests/Acceptance/installationTest.php'
        }
    }
}

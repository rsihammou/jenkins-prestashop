<?php
require 'vendor/autoload.php';
use PHPUnit\Framework\TestCase;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverSelect;
use Facebook\WebDriver\WebDriverExpectedCondition;

class PrestashopTest extends TestCase {

    private static $driver;
    private static $host_url;
    private static $storeTitle = "maBoutique";

    /**
     * Open the firefox browser just once to be reused on each test.
     */
    public static function setupBeforeClass(): void
    {
        $host = 'http://localhost:4444/wd/hub'; // this is the default port
        $driver = RemoteWebDriver::create($host, DesiredCapabilities::firefox());
        self::$driver = $driver;
        self::$host_url = getenv('HOST_URL');
    }

    /**
     * Before each test the same browser instance will be reused,
     * so clear cookies, local storage, etc., or open a new incognito tab
     */
    public function setUp(): void {}

    /**
     * After all tests have finished, quit the browser,
     * even if an exception was thrown and/or tests fail
     */
    public static function tearDownAfterClass(): void
    {
        // Close the browser
        self::$driver->quit();
    }

    /**
     * Verify prestashop installation processus
     */
    public function testPrestashopInstallation()
    {
        $driver = self::$driver;
        $driver->get(self::$host_url.'/install/index.php');
        // Wait until the language drop-down list is fully loaded.
        $driver->wait(10)->until(
            function () use ($driver) {
                $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('langList')));
                return count($select->getOptions()) == 44;
            },
            'Error unexpected number of items'
        );
        // Verify the page title
        $this->assertStringContainsString('PrestaShop Installation Assistant', $driver->getTitle());
        // Select English language
        $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('langList')));
        $select->selectByValue('en');
        // Click on the next button
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the license page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('licenses-agreement'))
        );
        // Verify the form title
        $this->assertStringContainsString('License Agreements',
                                          $driver->findElement(WebDriverBy::id("licenses-agreement"))
                                                 ->getText());
        // Validate license checkbox
        $driver->findElement(WebDriverBy::id("set_license"))->click();
        // Go to the next page
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the info store page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('infosShopBlock'))
        );
        // Verify the form title
        $this->assertStringContainsString('Information about your Store',
                                          $driver->findElement(WebDriverBy::cssSelector("#infosShopBlock h2"))
                                                 ->getText());
        // Add shop name
        $driver->findElement(WebDriverBy::id("infosShop"))->sendKeys(self::$storeTitle);
        // Choose shop activity
        $driver->findElement(WebDriverBy::cssSelector("#infosActivity_chosen span"))->click();
        $driver->findElement(WebDriverBy::cssSelector("#infosActivity_chosen .active-result:nth-child(3)"))->click();
        // Choose shop country
        $driver->findElement(WebDriverBy::cssSelector("#infosCountry_chosen span"))->click();
        $driver->findElement(WebDriverBy::cssSelector("#infosCountry_chosen .active-result:nth-child(2)"))->click();
        // Admin conf
        $driver->findElement(WebDriverBy::id("infosFirstname"))->sendKeys("admin");
        $driver->findElement(WebDriverBy::id("infosName"))->sendKeys("admin");
        $driver->findElement(WebDriverBy::id("infosEmail"))->sendKeys("admin@mail.fr");
        $driver->findElement(WebDriverBy::id("infosPassword"))->sendKeys("password");
        $driver->findElement(WebDriverBy::id("infosPasswordRepeat"))->sendKeys("password");
        // Go to the next page
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until the database configuration page is fully loaded
        $driver->wait(10)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('formCheckSQL'))
        );
        // Verify the form title
        $this->assertStringContainsString('Configure your database by filling out the following fields',
                                          $driver->findElement(WebDriverBy::cssSelector("#dbPart h2"))
                                                 ->getText());
        // Add database server
        $driver->findElement(WebDriverBy::id("dbServer"))->clear();
        $driver->findElement(WebDriverBy::id("dbServer"))->sendKeys("mysql");
        // Add database password
        $driver->findElement(WebDriverBy::id("dbPassword"))->sendKeys("prestashop");
        // Check database connection
        $driver->findElement(WebDriverBy::id("btTestDB"))->click();
        $driver->wait(10)->until(
            WebDriverExpectedCondition::elementTextIs(WebDriverBy::id('dbResultCheck'), 'Database is connected')
        );

        // Validate installation
        $driver->findElement(WebDriverBy::id("btNext"))->click();
        // Wait until completed installation
        $driver->wait(300)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id('install_process_success'))
        );
        // Check installation status
        $this->assertStringContainsString('Your installation is finished!',
                                          $driver->findElement(WebDriverBy::cssSelector("#install_process_success .clearfix h2"))
                                                 ->getText());
    }

    /**
     * Verify store
     */
    public function testMyStore()
    {
        $driver = self::$driver;
        $driver->get(self::$host_url.'/en/');
        // Wait until the index page is fully loaded and check the title
        $driver->wait(10)->until(
            WebDriverExpectedCondition::titleIs(self::$storeTitle)
        );
        // Check that the page contain 8 products
        $products = $driver->findElements(WebDriverBy::xpath("//article[contains(@class, 'product-miniature')]"));
        $this->assertCount(8, $products);
        // Click on the first product
        $driver->findElement(WebDriverBy::cssSelector(".product-miniature:nth-child(1) img"))->click();

        // Wait until the index page is fully loaded and check the title
        $driver->wait(10)->until(
            WebDriverExpectedCondition::titleIs("Hummingbird printed t-shirt")
        );

        // Select medium size
        $select = new WebDriverSelect($driver->findElement(WebDriverBy::id('group_1')));
        $select->selectByValue('2');
        // Select black color
        $driver->findElement(WebDriverBy::cssSelector(".float-xs-left:nth-child(2) .input-color"))->click();
        // Change product quantity
        $driver->findElement(WebDriverBy::cssSelector(".touchspin-up"))->click();
        // Add product to the cart
        $driver->findElement(WebDriverBy::cssSelector(".add-to-cart"))->click();

        // Wait until the cart modal will be displayed
        $driver->wait(10)->until(
            WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id("blockcart-modal"))
        );

        // Check the total of chosen products
        $this->assertStringContainsString('There are 2 items in your cart.',
                                           $driver->findElement(WebDriverBy::xpath("//div[@class='cart-content']/p[1]"))
                                                  ->getText());
        // Check the price
        $total = $driver->findElement(WebDriverBy::xpath("//div[@class='cart-content']/p[4]/span[2]"))->getText();
        $this->assertStringContainsString('€45.89',  $total);
    }
}
?>


